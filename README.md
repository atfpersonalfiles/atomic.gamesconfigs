# 🎮 atomic.gamesconfigs <!-- omit in toc -->
🎮 Configurations to some VALVe games that i play quite often.

*Read in another language: [Português](README.br.md).*

Download all files by pressing the *green button* `Code` and *download the ZIP*.

- [`"Semi-Automatic Method"` - Linux Only](#semi-automatic-method---linux-only)
  - [`Executing The Script`](#executing-the-script)
- [Manually Applying Each Configuration](#manually-applying-each-configuration)
  - [TF2 Configurations](#tf2-configurations)
      - [Copying TF2 Configs](#copying-tf2-configs)
      - [My Example](#my-example)
  - [CSGO Configurations](#csgo-configurations)
      - [Copying CSGO Configs](#copying-csgo-configs)
  - [For GMOD](#for-gmod)
    - [Symlinking In Linux](#symlinking-in-linux)
    - [Symlinking In Linux](#symlinking-in-linux-1)


# `"Semi-Automatic Method"` - Linux Only
> I created a script that do all three processess describing below speeding the process a little, althogh it isn't really that necessary.

> It's named "semi-automatic" because you, the user will need to change variable inside the file to make it work properly.

> There are some gimmicks that maybe more helpful than just doing everything manually so i guess that makes it a lot more worth it?

## `Executing The Script`
```
cd ~/atomic.gamesconfigs/
```
```
sudo chmod +X installConfigs
```
```
./installConfigs
```

# Manually Applying Each Configuration

## TF2 Configurations
*Startup Options*

`-dxlevel 98 -fullscreen -w [Your Preferred Width] -h [Your Preferred Height] -console -novid -high`

1. Go to your Steam folder and navigate the following path, folder after folder
2. Place the `cfg & custom` folders inside this repository into the folder inside your TF2 installation according to the instruction below.
    - steamapps > common > Team Fortress 2 > tf

#### Copying TF2 Configs
```
cp ~/atomic.gamesconfigs/TF2Config/* [YourSteamPath]/Steam/steamapps/common/Team Fortress 2/tf/
```
#### My Example
```
cp ~/atomic.gamesconfigs/TF2Config/ ~/.local/share/Steam/steamapps/common/Team Fortress 2/tf/
```

## CSGO Configurations
DISCLAIMERS: 
> *You'll need to open the game first to generate the CFG folder inside `userdata`.*

> You'll need to know your Steam ID. Pick the numbers after [U:1:(CopyOnlyTheNumbersHere)] at the steamID3 row. [SteamID](https://steamid.io/lookup/76561198329141402)

*Startup Options*

`-novid -freq 75 -tickrate 64 -fullscreen -w 1920 -h 1080 +exec autoexec.cfg`

1. Go to your Steam folder and navigate the following path, folder after folder:
2. Place the `autoexec.cfg` inside this repository into the folder corresponding to the instructions below.
    - `userdata > [Steam ID] > 730 > local > cfg`
3. Now, for both files `bhop.cfg & prac.cfg` put them into the folder inside your CSGO installation according to the instruction below.
    - `steamapps > common > CSGO > csgo > cfg`

#### Copying CSGO Configs
```
```

```
```

## For GMOD
*Startup Options*

`+exec autoexec.cfg`

1. Go to your Steam folder and navigate the following path, folder after folder:
2. Place the <code>autoexec.cfg</code> inside this repository into the folder corresponding to the instructions below.
    - `steamapps > GarrysMod > garrysmod > cfg`

### Symlinking In Linux 
*Assumes that you have a separe or external drive for games but can obviously be done in the main drive too:*

Changes Directory Into The GMOD Folder In My Repo
```
cd ~/atomic.gamesconfigs/GMODConfig/steamapps/common/GarrysMod/garrysmod
```

Symlinks It With The GarrysMod Path Installation Folder (Will Change According To Your Game Installation Path)

```
stow --target=[YourDrivePath]/SteamLibrary/steamapps/common/GarrysMod/garrysmod cfg
```

```
# My Example
stow --target=/run/media/atomicfeast/LinuxGames/SteamLibrary/steamapps/common/GarrysMod/garrysmod cfg
```

### Symlinking In Linux 
> There was a time where instructions about symlinking configurations were here. History is, it breaks Steam so i don't recommend you doing the same.
> Maybe it was something with my machine but i wouldn't risk it, discretion is advised.